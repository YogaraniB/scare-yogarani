package com.asminds.hibernatedemo.dao;

import com.asminds.hibernatedemo.Employee;

public interface Dao {
	public boolean insert(Employee emp);
}
