package com.asminds.hibernatedemo.daoimpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.asminds.hibernatedemo.Employee;
import com.asminds.hibernatedemo.dao.Dao;

public class DaoImpl implements Dao {

	@Override
	public boolean insert(Employee emp) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml"); 

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		
		Transaction t = session.beginTransaction();
		
		session.save(emp);
		t.commit();
		
		
		session.close();
		factory.close();
		System.out.println(" Data Has been saved");
		
		return true;
	}

}
