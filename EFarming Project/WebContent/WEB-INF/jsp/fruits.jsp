<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
function calculatePrice(){
    var price = $("#fruId").val();
    var qty = $("#fruQty").val();
    var total = qty*price;
    $("#fruRate").val(price);
    $("#totalPrice").val(total);
}
</script>
<body>
<form action="fru" method="post">
<center><h4>FRUIT</h4>

 Fruit:
 <select id="fruId" onchange="calculatePrice()" name=fruits> 
<option></option>
<option value="30">apple</option> 
<option value="50">Orange</option> 
<option value="50">Mango</option> 
<option value="25">Banana</option>
 <option value="20">papaya</option> 
 <option value="20">Strawberry</option> 
 <option value="10">Dates</option> 
<option value="55"> Graphs </option> 
<option value="22">lemon</option>
 <option value="80">Pomegranate</option> 
 <option value="50">Blueberry</option> 
 <option value="60">Watermelon</option> 
<option value="29">Fig</option> 
<option value="45">cherry</option>
 <option value="44">tomato</option> 
 </select><br><br>
 Qty:<select id="fruQty" onchange="calculatePrice()" name="Qty">
 <option></option>
 <option value="1">1 kg</option>
 <option value="2">2 kg</option>
 <option value="3">3 kg</option>
 <option value="4">4 kg</option>
 <option value="5">5 kg</option>
 <option value="6">6 kg</option>
 <option value="7">7 kg</option>
 <option value="8">8 kg</option>
 <option value="9">9 kg</option>
 <option value="10">10 kg</option>
 </select><br><br>
 Rate:<input type="number" id ="fruRate" name="rate" ><br><br>
 Total:<input type="number" id="totalPrice" name="total"><br><br>
 
<input type="submit" value="payment"><br><br>
<a href="Shopping">Add Items</a> 
</form>
</body>
</html>