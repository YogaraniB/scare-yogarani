<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
function calculatePrice(){
    var price = $("#fId").val();
    var qty = $("#fQty").val();
    var total = qty*price;
    $("#fRate").val(price);
    $("#totalPrice").val(total);
}
</script>
<body>
<form action="Flow" method="post">
<center><h4>FLOWER</h4>

 Flower:
 <select id="fId" onchange="calculatePrice()" name="flowers"> 
<option></option>
<option value="30">Lotus</option> 
<option value="50">Water Lily</option> 
<option value="50">Hibiscus</option> 
<option value="25">Lily</option>
 <option value="20">Rose</option> 
 <option value="20">Jasmine</option> 
 <option value="10">Daisy</option> 
<option value="55">Sunflower </option> 
<option value="22">Buttercup</option>
 <option value="80">Tulips</option> 
 </select><br><br>
 Qty:<select id="fQty" onchange="calculatePrice()" name="Qty">
 <option></option>
 <option value="1">1 kg</option>
 <option value="2">2 kg</option>
 <option value="3">3 kg</option>
 <option value="4">4 kg</option>
 <option value="5">5 kg</option>
 <option value="6">6 kg</option>
 <option value="7">7 kg</option>
 <option value="8">8 kg</option>
 <option value="9">9 kg</option>
 <option value="10">10 kg</option>
 </select><br><br>
 Rate:<input type="number" id ="fRate" name="rate" ><br><br>
 Total:<input type="number" id="totalPrice" name="total"><br><br>
 
<input type="submit" value="payment"><br><br>
 <a href="Shopping">Add Items</a> 
 </form>
</body>
</html>