<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Vegetables</title>
</head>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
function calculatePrice(){
    var price = $("#vegId").val();
    var qty = $("#vegQty").val();
    var total = qty*price;
    $("#vegRate").val(price);
    $("#totalPrice").val(total);
}
</script>
<body>
<form action="vveg" method="post">
<center><h4>VEGETABLES</h4>

 Vegetabes:
 <select id="vegId" onchange="calculatePrice()" name="Vegetabes" required> 
<option></option>
<option value="30">Carrot</option> 
<option value="50">broccoli</option> 
<option value="50">Ginger</option> 
<option value="25">cauliflower</option>
 <option value="20">corn</option> 
 <option value="20">cucumber</option> 
 <option value="10">Eggplant</option> 
<option value="55">green pepper </option> 
<option value="22">lettuce</option>
 <option value="80">mushrooms</option> 
 <option value="50">onion</option> 
 <option value="60">potato</option> 
<option value="29">pumpkin</option> 
<option value="45">red pepper</option>
 <option value="44">tomato</option> 
 <option value="40">beetroot</option> 
 <option value="30">brussel sprouts</option> 
<option value="20">peas</option> 
<option value="40">sweet potato</option>
 <option value="40">leek</option> 
 <option value="25">cabbage</option> 
<option value="25">chili</option> 
<option value="33">garlic</option> 
<option value="20">coriander</option>
 <option value="44">bean</option> 
 <option value="28">cinnamon</option> 
 </select><br><br>
 Qty:<select id="vegQty" onchange="calculatePrice()" name="Qty" required>
 <option></option>
 <option value="1">1 kg</option>
 <option value="2">2 kg</option>
 <option value="3">3 kg</option>
 <option value="4">4 kg</option>
 <option value="5">5 kg</option>
 <option value="6">6 kg</option>
 <option value="7">7 kg</option>
 <option value="8">8 kg</option>
 <option value="9">9 kg</option>
 <option value="10">10 kg</option>
 </select><br><br>
 Rate:<input type="number" id ="vegRate" name="rate" ><br><br>
 Total:<input type="number" id="totalPrice" name="total"><br><br>
 
<input type="submit" value="payment"><br><br>
<a href="Shopping">Add Items</a> 
</form>
</body>
</html>