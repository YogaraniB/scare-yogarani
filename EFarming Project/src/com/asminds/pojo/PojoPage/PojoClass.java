package com.asminds.pojo.PojoPage;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "NEWUSER")
public class PojoClass {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String name;
	private String email;
	private String password;
	private String pnumber;
	private String address;
	private String city;
	private String state;
	private String landmark;
	//private String post;
	//private String pincode;
	
	public PojoClass()
	{
		
	}
	public PojoClass(int id, String name, String email, String password, String pnumber, String address, String city,
			String pincode, String state, String landmark, String post) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.pnumber = pnumber;
		this.address = address;
		this.city = city;
		//this.pincode = pincode;
		this.state = state;
		this.landmark = landmark;
		//this.post = post;
	}
	
	public PojoClass(String name, String email, String password, String pnumber, String address, String city,
			String pincode, String state, String landmark, String post) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.pnumber = pnumber;
		this.address = address;
		this.city = city;
		//this.pincode = pincode;
		this.state = state;
		this.landmark = landmark;
		//this.post = post;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPnumber() {
		return pnumber;
	}
	public void setPnumber(String pnumber) {
		this.pnumber = pnumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	/*public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}*/
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	/*public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}*/
	

}
