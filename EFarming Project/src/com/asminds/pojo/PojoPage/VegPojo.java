package com.asminds.pojo.PojoPage;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name="VegetableProduct")
public class VegPojo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private int Id;
	private String Vegetabes;
	private String Qty;
	private String rate;
	private String total;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	
	public String getVegetabes() {
		return Vegetabes;
	}
	public void setVegetabes(String vegetabes) {
		Vegetabes = vegetabes;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	
	public VegPojo()
	{
		
	}
	
	public VegPojo(String vegetabes, String qty, String rate, String total) {
		super();
		Vegetabes = vegetabes;
		Qty = qty;
		this.rate = rate;
		this.total = total;
	}
	public VegPojo(int id, String vegetabes, String qty, String rate, String total) {
		super();
		Id = id;
		Vegetabes = vegetabes;
		Qty = qty;
		this.rate = rate;
		this.total = total;
	}

	
}
