package com.asminds.pojo.PojoPage;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
@Entity
@Table(name="FlowerProduct")

public class FlowPojo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;
	private String flowers;
	private String Qty;
	private String rate;
	private String total;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getFlowers() {
		return flowers;
	}
	public void setFlowers(String flowers) {
		this.flowers = flowers;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	
	public FlowPojo()
	{
		
	}
	
	public FlowPojo(String flowers, String qty, String rate, String total) {
		super();
		this.flowers = flowers;
		Qty = qty;
		this.rate = rate;
		this.total = total;
	}
	public FlowPojo(int id, String flowers, String qty, String rate, String total) {
		super();
		Id = id;
		this.flowers = flowers;
		Qty = qty;
		this.rate = rate;
		this.total = total;
	}
	
	

}
