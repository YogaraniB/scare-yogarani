package com.asminds.OnlineDoctorAppointment.DAOImplementation;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.asminds.OnlineDoctorAppointment.DAO.DoctorDAO;
import com.asminds.OnlineDoctorAppointment.Pojo.DoctorPojo;

public class DoctorDAOImplementation implements DoctorDAO {
	@Override
	public boolean save(DoctorPojo p) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(p);
		tx.commit();
		session.close();
		factory.close();
		System.out.println("Object Saved Successfully");
		return true;
	}

	@Override
	public List<DoctorPojo> viewAll() {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Query qry = session.createQuery("from DoctorPojo p");
		List <DoctorPojo> l = qry.list();
		System.out.println("Total Number Of Records : " + l.size());
		Iterator it = l.iterator();

		while (it.hasNext()) {
			Object o = (Object) it.next();
			DoctorPojo p = (DoctorPojo) o;
			System.out.println(p);

		}
		return l;
	}
	public List<DoctorPojo> viewbySpecialist(DoctorPojo emp) {
		// TODO Auto-generated method stub
		
		System.out.println("I amin Viewbyspecialist implementation");
		System.out.println(emp.getDoctorname());
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml"); 

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
	
		String hql = "select * from DoctorPojo where specialist =" +emp.getSpecialist();
		System.out.println(hql);
		Query qry = session.createQuery("from DoctorPojo where specialist="+emp.getSpecialist());
		List l =qry.list();
		System.out.println("Total Number Of Records : "+l.size());
		java.util.Iterator it = l.iterator();

		while(it.hasNext())
		{
		
			DoctorPojo reg = (DoctorPojo)it.next();
			System.out.println(reg.getDoctorname());
			System.out.println(reg.getEmailId());
			System.out.println(reg.getSpecialist());
		}
		session.close();
		factory.close();
		return l;
	}

	
	public List<DoctorPojo> validateUser(DoctorPojo login) {
		System.out.println(login.getDoctorname() +" " +login.getPassword());
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Query qry = session.createQuery("from DoctorPojo where doctorname=:doctorname and password=:password");
		qry.setParameter("doctorname", login.getDoctorname());
		qry.setParameter("password", login.getPassword());
		List l = qry.list();
		System.out.println("Total Number Of Records : " + l.size());
		Iterator it = l.iterator();

		while (it.hasNext()) 
		{
			Object o = (Object) it.next();
			DoctorPojo p = (DoctorPojo) o;
			System.out.println("DoctorName : " + p.getDoctorname());
			System.out.println("Password : " + p.getPassword());
			System.out.println("----------------------");
		}
		session.close();
		factory.close();
		return l;
	}
}
