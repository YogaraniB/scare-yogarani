package com.asminds.OnlineDoctorAppointment.DAOImplementation;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.asminds.OnlineDoctorAppointment.DAO.GasbookingDAO;
import com.asminds.OnlineDoctorAppointment.Pojo.GasbookingPojo;

public class GasbookingDAOImplementation implements GasbookingDAO {

	@Override
	public boolean save(GasbookingPojo p) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(p);
		tx.commit();
		session.close();
		factory.close();
		System.out.println("Object Saved Successfully");
		return true;
	}

	@Override
	public List<GasbookingPojo> viewAll() {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Query qry = session.createQuery("from GasbookingPojo p");
		List <GasbookingPojo> l = qry.list();
		System.out.println("Total Number Of Records : " + l.size());
		Iterator it = l.iterator();

		while (it.hasNext()) {
			Object o = (Object) it.next();
			GasbookingPojo p = (GasbookingPojo) o;
			System.out.println(p);

		}
		return l;
	}

	

}
