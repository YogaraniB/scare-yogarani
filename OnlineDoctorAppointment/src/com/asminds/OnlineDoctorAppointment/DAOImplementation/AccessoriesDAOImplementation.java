package com.asminds.OnlineDoctorAppointment.DAOImplementation;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.asminds.OnlineDoctorAppointment.DAO.AccessoriesDAO;
import com.asminds.OnlineDoctorAppointment.Pojo.AccessoriesPojo;


public class AccessoriesDAOImplementation implements AccessoriesDAO {

	@Override
	public boolean save(AccessoriesPojo p) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(p);
		tx.commit();
		session.close();
		factory.close();
		System.out.println("Object Saved Successfully");
		return true;
	}

	@Override
	public List<AccessoriesPojo> viewAll() {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Query qry = session.createQuery("from AccessoriesPojo p");
		List <AccessoriesPojo> l = qry.list();
		System.out.println("Total Number Of Records : " + l.size());
		Iterator it = l.iterator();

		while (it.hasNext()) {
			Object o = (Object) it.next();
			AccessoriesPojo p = (AccessoriesPojo) o;
			System.out.println(p);

		}
		return l;
	}

}
