package com.asminds.OnlineDoctorAppointment.DAOImplementation;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.asminds.OnlineDoctorAppointment.DAO.PatientDAO;
import com.asminds.OnlineDoctorAppointment.Pojo.PatientPojo;

public class PatientDAOImplementation implements PatientDAO {

	@Override
	public boolean save(PatientPojo p) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(p);
		tx.commit();
		session.close();
		factory.close();
		System.out.println("Object Saved Successfully");
		return true;
	}
@Override
	public List<PatientPojo> viewAll() {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Query qry = session.createQuery("from PatientPojo p");
		List <PatientPojo> l = qry.list();
		System.out.println("Total Number Of Records : " + l.size());
		Iterator it = l.iterator();

		while (it.hasNext()) {
			Object o = (Object) it.next();
			PatientPojo p = (PatientPojo) o;
			System.out.println(p);

		}
		return l;
	}

	public List<PatientPojo> validateUser(PatientPojo login) {
		System.out.println(login.getpatientname() +" " +login.getPassword());
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Query qry = session.createQuery("from PatientPojo where patientname=:patientname and password=:password");
		qry.setParameter("patientname", login.getpatientname());
		qry.setParameter("password", login.getPassword());
		List l = qry.list();
		System.out.println("Total Number Of Records : " + l.size());
		Iterator it = l.iterator();

		while (it.hasNext()) 
		{
			Object o = (Object) it.next();
			PatientPojo p = (PatientPojo) o;
			System.out.println("patientName : " + p.getpatientname());
			System.out.println("Password : " + p.getPassword());
			System.out.println("----------------------");
		}
		session.close();
		factory.close();
		return l;
	}

}
