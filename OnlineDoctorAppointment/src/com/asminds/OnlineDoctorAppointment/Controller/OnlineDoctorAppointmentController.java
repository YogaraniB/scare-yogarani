package com.asminds.OnlineDoctorAppointment.Controller;

import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.asminds.OnlineDoctorAppointment.DAOImplementation.AccessoriesDAOImplementation;
import com.asminds.OnlineDoctorAppointment.DAOImplementation.ConnectionDAOImplementation;
import com.asminds.OnlineDoctorAppointment.DAOImplementation.DoctorDAOImplementation;
import com.asminds.OnlineDoctorAppointment.DAOImplementation.FeedbackDAOImplementation;
import com.asminds.OnlineDoctorAppointment.DAOImplementation.GasbookingDAOImplementation;
import com.asminds.OnlineDoctorAppointment.DAOImplementation.PatientDAOImplementation;
import com.asminds.OnlineDoctorAppointment.Pojo.AccessoriesPojo;
import com.asminds.OnlineDoctorAppointment.Pojo.ConnectionPojo;
import com.asminds.OnlineDoctorAppointment.Pojo.DoctorPojo;
import com.asminds.OnlineDoctorAppointment.Pojo.FeedbackPojo;
import com.asminds.OnlineDoctorAppointment.Pojo.GasbookingPojo;
import com.asminds.OnlineDoctorAppointment.Pojo.LoginPojo;
import com.asminds.OnlineDoctorAppointment.Pojo.PatientPojo;


@Controller
public class OnlineDoctorAppointmentController {
	@RequestMapping("/")
	public String index() {
		System.out.println("I am in index page");
		return "index";

	}
	@RequestMapping("/index")
	public String index1() {
		System.out.println("I am in index page");
		return "index";

	}
	@RequestMapping("/logout")
	public String logout() {
		System.out.println("I am in logout page");
		return "index";

	}
	@RequestMapping(value = "/formvalidationadmin", method = RequestMethod.POST)
	public ModelAndView formvalidation(@ModelAttribute("userform") LoginPojo user) {
		String username = user.getUsername();
		String password = user.getPassword();
		String wrong="Sorry! Invalid Username or Password...";
		if (username.equals("admin") && password.equals("123")) {
			return new ModelAndView("adminpage");
		} else {
			return new ModelAndView("index","wrong",wrong);
		}

	}
	@RequestMapping("/adminpage")
	public String adminpage() {
		System.out.println("I am in admin page");
		return "adminpage";

	}
	@RequestMapping("/userpage")
	public String userpage() {
		System.out.println("I am in user page");
		return "doctorpage";

	}
	@RequestMapping("adminlogin")
	public String adminlogin() {
		System.out.println("I am in admin login page");
		return "adminloginpage";

	}
	@RequestMapping("doctorlogin")
	public String Doctorlogin() {
		System.out.println("I am in doctor login page");
		return "doctorloginpage";

	}
	@RequestMapping("patientlogin")
	public String patientlogin() {
		System.out.println("I am in patient login page");
		return "patientloginpage";

	}
	@RequestMapping("/register")
	public String register() {
		System.out.println("i am in register page");
		return "register";
	}
	@RequestMapping("/registerpatient")
	public String registerpatient() {
		System.out.println("i am in register patient page");
		return "registerpatient";
	}
	@RequestMapping(value = "/formvalidationdoctor", method = RequestMethod.POST)
	public ModelAndView formvalidationDoctor(@ModelAttribute("userform") DoctorPojo user) {
		DoctorDAOImplementation i = new DoctorDAOImplementation();
		System.out.println("I am in validation page");
		List<DoctorPojo> v = i.validateUser(user);
		System.out.println("I am in Validating a Password");
		Iterator it = v.iterator();
		String a=user.getDoctorname();
		if (v.size() != 0) {
			return new ModelAndView("doctorpage","a",a);
		} else {
			return new ModelAndView("index");
		}

	}
	@RequestMapping(value = "/formvalidationpatient", method = RequestMethod.POST)
	public ModelAndView formvalidationpatient(@ModelAttribute("userform") PatientPojo user) {
		PatientDAOImplementation i = new PatientDAOImplementation();
		System.out.println("I am in validation page");
		List<PatientPojo> v = i.validateUser(user);
		String a=user.getpatientname();
		System.out.println("I am in Validating a Password");
		Iterator it = v.iterator();
		String wrong="Sorry! Invalid Username or Password...";
		if (v.size() != 0) {
			return new ModelAndView("patientpage","a",a);
		} else {
			return new ModelAndView("index","wrong",wrong);
		}

	}
	@RequestMapping("/registersave")
	public ModelAndView registersave(@ModelAttribute("z") DoctorPojo i) {
		System.out.println("i am in register save action");
		System.out.println(i.getDoctorid());
		System.out.println(i.getEmailId());
		System.out.println(i.getPassword());
		System.out.println(i.getDoctorname());

		DoctorDAOImplementation l = new DoctorDAOImplementation();
		l.save(i);
		String s = i.getDoctorname();
		String p=i.getSpecialist();
		return new ModelAndView("doctorpage", "a", s);
	}
	@RequestMapping("/registersavepatient")
	public ModelAndView registersavepatient(@ModelAttribute("z") PatientPojo i) {
		System.out.println("i am in register save action");
		System.out.println(i.getpatientid());
		System.out.println(i.getEmailId());
		System.out.println(i.getPassword());
		System.out.println(i.getpatientname());

		PatientDAOImplementation l = new PatientDAOImplementation();
		l.save(i);
		String s = i.getpatientname();
		return new ModelAndView("patientpage", "a", s);
	}
	@RequestMapping("/patientpage")
	public String patientpage() {
		System.out.println("i am in patient page");
		return "patientpage";
	}
	@RequestMapping("/doctorpage")
	public String doctorpage() {
		System.out.println("i am in doctor page");
		return "doctorpage";
	}
	@RequestMapping("/searchdoctors")
	public String newconnection() {
		System.out.println("i am in search doctors page");
		return "searchdoctors";
	}
	@RequestMapping("/searcheddoctors")
	public ModelAndView connectionsave(@ModelAttribute("z") DoctorPojo i) {
		System.out.println("i am in searched doctor action");
		System.out.println(i.getDoctorname());
		System.out.println(i.getEmailId());
		System.out.println(i.getExperience());

		DoctorDAOImplementation a = new DoctorDAOImplementation();
		List<DoctorPojo> b = a.viewAll();
		return new ModelAndView("searcheddoctors", "userList", b);
		
	}
	@RequestMapping("/gasbookingsave")
	public ModelAndView gasbookingsave(@ModelAttribute("z") GasbookingPojo i) {
		System.out.println("i am in gas booking save action");
		System.out.println(i.getDistributor());
		System.out.println(i.getFullname());
		System.out.println(i.getEmail());

		GasbookingDAOImplementation l = new GasbookingDAOImplementation();
		l.save(i);
		return new ModelAndView("doctorpage");
	}
	
	@RequestMapping("/feedback")
	public String feedback() {
		System.out.println("i am in feedback page");
		return "feedback";
	}
	@RequestMapping("/feedbacksave")
	public ModelAndView feedbacksave(@ModelAttribute("z") FeedbackPojo i) {
		System.out.println("i am in feedback save action");
		System.out.println(i.getDoctorname());
		System.out.println(i.getFullname());
		System.out.println(i.getEmail());

		FeedbackDAOImplementation l = new FeedbackDAOImplementation();
		l.save(i);
		return new ModelAndView("patientpage");
	}
	@RequestMapping("/book")
	public ModelAndView book(@ModelAttribute("t") DoctorPojo p) {
		System.out.println("i am in booking page");
		DoctorDAOImplementation a = new DoctorDAOImplementation();
		List<DoctorPojo> b = a.viewbySpecialist(p);
		return new ModelAndView ("book" , "b",b);
	}
	@RequestMapping("/viewdoctorslist")
	public ModelAndView viewnewconnection(@ModelAttribute("t") DoctorPojo p) {
		System.out.println("I am in view new connection page");
		DoctorDAOImplementation a = new DoctorDAOImplementation();
		List<DoctorPojo> b = a.viewAll();
		return new ModelAndView("fetchdoctorslist", "userList", b);

	}
	@RequestMapping("/viewpatientslist")
	public ModelAndView viewnewgasbooking(@ModelAttribute("t") PatientPojo p) {
		System.out.println("I am in view new gas booking page");
		PatientDAOImplementation a = new PatientDAOImplementation();
		List<PatientPojo> b = a.viewAll();
		return new ModelAndView("fetchpatientslist", "userList", b);

	}@RequestMapping("/viewaccessories")
	public ModelAndView viewnewaccessories(@ModelAttribute("t") AccessoriesPojo p) {
		System.out.println("I am in view new accessories page");
		AccessoriesDAOImplementation a = new AccessoriesDAOImplementation();
		List<AccessoriesPojo> b = a.viewAll();
		return new ModelAndView("fetchnewaccessories", "userList", b);

	}@RequestMapping("/viewfeedback")
	public ModelAndView viewnewfeedback(@ModelAttribute("t") FeedbackPojo p) {
		System.out.println("I am in view new feedback page");
		FeedbackDAOImplementation a = new FeedbackDAOImplementation();
		List<FeedbackPojo> b = a.viewAll();
		return new ModelAndView("fetchfeedback", "userList", b);

	}
	@RequestMapping("/viewfeedback2")
	public ModelAndView viewnewfeedback2(@ModelAttribute("t") FeedbackPojo p) {
		System.out.println("I am in view new feedback page");
		FeedbackDAOImplementation a = new FeedbackDAOImplementation();
		List<FeedbackPojo> b = a.viewAll();
		return new ModelAndView("fetchfeedback2", "userList", b);

	}
	@RequestMapping("/contactus")
	public String contactus() {
		System.out.println("i am in contact page");
		return "contactus";
	}
}
