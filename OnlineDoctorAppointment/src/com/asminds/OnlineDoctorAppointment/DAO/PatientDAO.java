package com.asminds.OnlineDoctorAppointment.DAO;

import java.util.List;

import com.asminds.OnlineDoctorAppointment.Pojo.PatientPojo;


public interface PatientDAO {
	public boolean save(PatientPojo p);
	public List<PatientPojo> viewAll();
	public List<PatientPojo> validateUser(PatientPojo user);
}
