package com.asminds.OnlineDoctorAppointment.DAO;

import java.util.List;

import com.asminds.OnlineDoctorAppointment.Pojo.GasbookingPojo;



public interface GasbookingDAO {
	public boolean save(GasbookingPojo p);
	public List<GasbookingPojo> viewAll();
	
}
