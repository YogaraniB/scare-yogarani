package com.asminds.OnlineDoctorAppointment.DAO;

import java.util.List;

import com.asminds.OnlineDoctorAppointment.Pojo.AccessoriesPojo;


public interface AccessoriesDAO {
	public boolean save(AccessoriesPojo p);
	public List<AccessoriesPojo> viewAll();
}
