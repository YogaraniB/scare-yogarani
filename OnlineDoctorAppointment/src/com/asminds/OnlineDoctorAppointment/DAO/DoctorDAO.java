package com.asminds.OnlineDoctorAppointment.DAO;

import java.util.List;

import com.asminds.OnlineDoctorAppointment.Pojo.DoctorPojo;

public interface DoctorDAO {
	public boolean save(DoctorPojo p);
	public List<DoctorPojo> viewAll();
	public List<DoctorPojo> validateUser(DoctorPojo user);
	public List<DoctorPojo> viewbySpecialist(DoctorPojo emp);
}
