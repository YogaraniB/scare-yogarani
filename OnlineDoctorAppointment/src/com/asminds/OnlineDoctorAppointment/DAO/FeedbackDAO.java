package com.asminds.OnlineDoctorAppointment.DAO;

import java.util.List;

import com.asminds.OnlineDoctorAppointment.Pojo.FeedbackPojo;





public interface FeedbackDAO {

	public boolean save(FeedbackPojo p);
	public List<FeedbackPojo> viewAll();
	
}
