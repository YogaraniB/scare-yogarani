package com.asminds.OnlineDoctorAppointment.DAO;

import java.util.List;

import com.asminds.OnlineDoctorAppointment.Pojo.ConnectionPojo;



public interface ConnectionDAO {
	public boolean save(ConnectionPojo p);
	public List<ConnectionPojo> viewAll();
}
