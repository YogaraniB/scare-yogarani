package com.asminds.OnlineDoctorAppointment.Pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AccessoriesPojo {
@Id
	String email;
String country;
String state;;
String district;
String distributor;
String fullname;
String mobileno;
String cylinderaccessories;

public AccessoriesPojo(String email, String country,String state, String district, String distributor, String fullname,
		String mobileno,String cylinderaccessories) {
	super();
	this.email = email;
	this.country=country;
	this.state = state;
	this.district = district;
	this.distributor = distributor;
	this.fullname = fullname;
	this.mobileno = mobileno;
	this.cylinderaccessories=cylinderaccessories;
}

public String getCylinderaccessories() {
	return cylinderaccessories;
}

public void setCylinderaccessories(String cylinderaccessories) {
	this.cylinderaccessories = cylinderaccessories;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getState() {
	return state;
}

public void setState(String state) {
	this.state = state;
}

public String getCountry() {
	return country;
}

public void setCountry(String country) {
	this.country = country;
}

public String getDistrict() {
	return district;
}

public void setDistrict(String district) {
	this.district = district;
}

public String getDistributor() {
	return distributor;
}

public void setDistributor(String distributor) {
	this.distributor = distributor;
}

public String getFullname() {
	return fullname;
}

public void setFullname(String fullname) {
	this.fullname = fullname;
}

public String getMobileno() {
	return mobileno;
}

public void setMobileno(String mobileno) {
	this.mobileno = mobileno;
}

public AccessoriesPojo() {
	super();
}

}
