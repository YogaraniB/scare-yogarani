package com.asminds.OnlineDoctorAppointment.Pojo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class GasbookingPojo {
@Id
	String email;
String country;
String state;
String district;
String distributor;
int serialcardno;
String fullname;
String mobileno;

public GasbookingPojo(String email,String country, String state, String district, String distributor, int serialcardno,
		String fullname, String mobileno) {
	super();
	this.email = email;
	this.country=country;
	this.state = state;
	this.district = district;
	this.distributor = distributor;
	this.serialcardno = serialcardno;
	this.fullname = fullname;
	this.mobileno = mobileno;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getState() {
	return state;
}

public void setState(String state) {
	this.state = state;
}

public String getDistrict() {
	return district;
}

public void setDistrict(String district) {
	this.district = district;
}

public String getDistributor() {
	return distributor;
}

public void setDistributor(String distributor) {
	this.distributor = distributor;
}

public int getSerialcardno() {
	return serialcardno;
}

public void setSerialcardno(int serialcardno) {
	this.serialcardno = serialcardno;
}

public String getFullname() {
	return fullname;
}

public void setFullname(String fullname) {
	this.fullname = fullname;
}

public String getMobileno() {
	return mobileno;
}

public void setMobileno(String mobileno) {
	this.mobileno = mobileno;
}

public String getCountry() {
	return country;
}

public void setCountry(String country) {
	this.country = country;
}

public GasbookingPojo() {
	super();
}

}