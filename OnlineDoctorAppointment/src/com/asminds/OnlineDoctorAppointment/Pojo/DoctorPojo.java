package com.asminds.OnlineDoctorAppointment.Pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity 
public class DoctorPojo {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	int doctorid;
String doctorname;
String specialist;
String password;
String emailId;
String gender;
String mobileno;
String address;
String city;
int fees;
int experience;

public String getSpecialist() {
	return specialist;
}
public void setSpecialist(String specialist) {
	this.specialist = specialist;
}
public int getDoctorid() {
	return doctorid;
}
public void setDoctorid(int doctorid) {
	this.doctorid = doctorid;
}
public String getDoctorname() {
	return doctorname;
}
public void setDoctorname(String doctorname) {
	this.doctorname = doctorname;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getMobileno() {
	return mobileno;
}
public void setMobileno(String mobileno) {
	this.mobileno = mobileno;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public int getFees() {
	return fees;
}
public void setFees(int fees) {
	this.fees = fees;
}
public int getExperience() {
	return experience;
}
public void setExperience(int experience) {
	this.experience = experience;
}
public DoctorPojo(int doctorid, String doctorname,String specialist, String password, String emailId, String gender, String mobileno,
		String address, String city, int fees, int experience) {
	super();
	this.doctorid = doctorid;
	this.doctorname = doctorname;
	this.specialist = specialist;
	this.password = password;
	this.emailId = emailId;
	this.gender = gender;
	this.mobileno = mobileno;
	this.address = address;
	this.city = city;
	this.fees = fees;
	this.experience = experience;
}
public DoctorPojo() {
	super();
}

}
