package com.asminds.OnlineDoctorAppointment.Pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity 
public class PatientPojo {
@Id @GeneratedValue(strategy=GenerationType.AUTO)
	int patientid;
String patientname;
String password;
String emailId;
String gender;
String mobileno;
String address;
String city;
String state;
public PatientPojo(int patientid, String patientname, String password, String emailId, String gender,
		String mobileno, String address, String city, String state) {
	super();
	this.patientid = patientid;
	this.patientname = patientname;
	this.password = password;
	this.emailId = emailId;
	this.gender = gender;
	this.mobileno = mobileno;
	this.address = address;
	this.city = city;
	this.state = state;
}
public int getpatientid() {
	return patientid;
}
public void setpatientid(int patientid) {
	this.patientid = patientid;
}
public String getpatientname() {
	return patientname;
}
public void setpatientname(String patientname) {
	this.patientname = patientname;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getMobileno() {
	return mobileno;
}
public void setMobileno(String mobileno) {
	this.mobileno = mobileno;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public PatientPojo() {
	super();
}



}
