<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><link rel="stylesheet" href="styles.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
body {
  background-color:hsla(0, 100%, 70%, 0.3);
  font-family:sans-serif;
 
 
}
h2 {
  color: blue;
  text-shadow: -1px 0 white, 0 1px black, 1px 0 black, 0 -1px black;
  font-family: sans-serif;
}
a:link {
  
  font-family: sans-serif;
}

a:visited {
  background-color: white;
 font-family: sans-serif;
}

a:hover {
  background-color: white;
  font-family:sans-serif;
}

a:active {
  background-color: hotpink;
  font-family: sans-serif;
} 
</style>
<title>Patient Login Page</title>
</head>
<body>
<form action="formvalidationpatient" method="post">
<a href="index"> Back to Home</a><br><br>
<center>
		<u> <h2> Patient login</h2></u>
	</center>
	
		<h3>Patient Name: <input type="text" name="patientname"><br><br> </h3>
		<h3>Password: <input type="password" name="password"><br> </h3>
		<br>
		
		<h4>
			<input type="submit"  value="Login" />
		</h4>
<a href="registerpatient">New Patient?Register Here</a>
	</form>
</body>
</html>