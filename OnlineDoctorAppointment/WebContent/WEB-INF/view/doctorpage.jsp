<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><link rel="stylesheet" href="styles.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<style type="text/css">
body {
  background-color:hsla(0, 100%, 70%, 0.3);
  font-family:sans-serif;
 
}
h1 {
  color: blue;
  font-family: sans-serif;
}
a:link {
  
  font-family: sans-serif;
}

a:visited {
  background-color: white;
 font-family: sans-serif;
}

a:hover {
  background-color: white;
  font-family:sans-serif;
}

a:active {
  background-color: hotpink;
  font-family: sans-serif;
} </style>
<title>Doctor Page</title>
</head>
<body>
<a href="index"> Back to Home</a><br><br>
<h2>Welcome ${a}, </h2>
<!-- <a href="bookingrequest">Booking request</a> &nbsp; &nbsp; &nbsp; &nbsp; -->
<a href="viewfeedback2">View Feedback</a> &nbsp; &nbsp; &nbsp; &nbsp;
<a href="logout">Log out</a> &nbsp; &nbsp; &nbsp; &nbsp;
</body>
</html>