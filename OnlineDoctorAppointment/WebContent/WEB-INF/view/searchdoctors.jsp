<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><link rel="stylesheet" href="styles.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
body {
  background-color:hsla(0, 100%, 70%, 0.3);
  font-family:sans-serif;
 
 
}
h2 {
  color: blue;
  text-shadow: -1px 0 white, 0 1px black, 1px 0 black, 0 -1px black;
  font-family: sans-serif;
}
a:link {
  
  font-family: sans-serif;
}

a:visited {
  background-color: white;
 font-family: sans-serif;
}

a:hover {
  background-color: white;
  font-family:sans-serif;
}

a:active {
  background-color: hotpink;
  font-family: sans-serif;
} 
</style>
<script>
var stateObject = {
"India": { "Delhi": ["new Delhi", "North Delhi"],
"Kerala": ["Thiruvananthapuram"," Kollam"," Alappuzha"," Pathanamthitta"," Kottayam"," Idukki"," Ernakulam"," Thrissur"," Palakkad"," Malappuram"," Kozhikode",
	"Wayanadu"," Kannur","Kasaragod"],
"Goa": [ "South Goa","Tiswadi (Panaji)", "Bardez (Mapusa)"," Pernem, Bicholim","Sattari (Valpoi)"],
"TamilNadu": ["Thanjavur", "Madurai" ,"Trichy" ,"Kumbakonam" ,"Nagapattinam","Kanyakumari",
	"Madurai","Ramanathapuram","Tirunelveli",
	"Pudukkottai","Virudhunagar","Sivagangai","Dindigul","Thoothukudi","Theni"],
"Telangana": ["Hyderabad", "Visakapatnam" ,"Mahbubnagar district", "Medak district"," Nalgonda district (Nalgundah)",
	" Nizamabad districts"," Adilabad, Karimnagar"," Warangal"],
},
"Australia": {
"South Australia": ["Dunstan", "Mitchell"],
"Victoria": ["Altona", "Euroa"]
}, "Canada": {
"Alberta": ["Acadia", "Bighorn"],
"Columbia": ["Washington", ""]
},
}
window.onload = function () {
var countySel = document.getElementById("countySel"),
stateSel = document.getElementById("stateSel"),
districtSel = document.getElementById("districtSel");
for (var country in stateObject) {
countySel.options[countySel.options.length] = new Option(country, country);
}
countySel.onchange = function () {
stateSel.length = 1; // remove all options bar first
districtSel.length = 1; // remove all options bar first
if (this.selectedIndex < 1) return; // done 
for (var state in stateObject[this.value]) {
stateSel.options[stateSel.options.length] = new Option(state, state);
}
}
countySel.onchange(); // reset in case page is reloaded
stateSel.onchange = function () {
districtSel.length = 1; // remove all options bar first
if (this.selectedIndex < 1) return; // done 
var district = stateObject[countySel.value][this.value];
for (var i = 0; i < district.length; i++) {
districtSel.options[districtSel.options.length] = new Option(district[i], district[i]);
}
}
}
</script>
<title>Search doctors</title>
</head>
<body >
	<a href="patientpage"> Back to Home</a><br><br>
	<form action="searcheddoctors" method="post">


		<h2>Search doctors</h2>
		Category: &nbsp; <input type="text" placeholder="category" name="category"
					required> <br><br>
		Reason: &nbsp; <input type="text" placeholder="reason" name="reason"
					required> <br><br>
		Location: <br><textarea rows="4" cols="50" name="location"></textarea>
		<br><br>
			
	

		<input type="submit" value="Submit"> <input type="reset"
			value="reset"><br>

		
	</form>
</body>
</html>