<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><link rel="stylesheet" href="styles.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<style type="text/css">
body {
  background-color:hsla(0, 100%, 70%, 0.3);
  font-family:sans-serif;
 
}
h1 {
  color: blue;
  font-family: sans-serif;
}
a:link {
  
  font-family: sans-serif;
}

a:visited {
  background-color: white;
 font-family: sans-serif;
}

a:hover {
  background-color: white;
  font-family:sans-serif;
}

a:active {
  background-color: hotpink;
  font-family: sans-serif;
} </style>
<title>Doctor Page</title>
</head>
<body>
<a href="index"> Back to Home</a><br><br>
<form action="bookingsave" method="post">
<br><br>
Doctor Name: &nbsp; <input type="text"  name="doctorname" ${b.doctorname }
					required> <br><br>
			
			
				Email-Id: &nbsp; <input type="email" placeholder="email" name="emailId"
					required><br><br>
					
				Specialist: &nbsp; <input type="text" placeholder="specialist" name="specialist"
					required><br><br>
			
		
				Gender: &nbsp; 	 <input type="radio" name="gender" value="male" checked> Male<br>
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="radio" name="gender" value="female"> Female<br>
  <br><br>
			
				Password: &nbsp; <input type="password" placeholder="new password" name="password" maxlength="20" required>
			<br><br>
			
				Mobile No: &nbsp; <input type="number" placeholder="Mobile Number" name="mobileno" maxlength="10" required>
			<br><br>
			
				Address: &nbsp; <input type="text" placeholder="Address" name="address"
					required>
				<br><br>
			
				City:  &nbsp; <input type="text" placeholder="City" name="city" required>
			<br><br>
			
				State: &nbsp; <input type="text" placeholder="State" name="state"
					required>
			<br><br>
			Fees: &nbsp; <input type="number" placeholder="fees" name="fees"
					required>
			<br><br>
			Experience: &nbsp; <input type="number" placeholder="experience" name="experience"
					required>
			<br><br>
			<input type="submit" onclick="myFunction()" value="Submit"> <input type="reset"
			value="reset"><br>
			</form>

</body>
</html>