<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><style type="text/css">
body {
  background-color:hsla(0, 100%, 70%, 0.3);
  font-family:sans-serif;
 
 
}
h2 {
  color: blue;
  text-shadow: -1px 0 white, 0 1px black, 1px 0 black, 0 -1px black;
  font-family: sans-serif;
}
a:link {
  
  font-family: sans-serif;
}

a:visited {
  background-color: white;
 font-family: sans-serif;
}

a:hover {
  background-color: white;
  font-family:sans-serif;
}

a:active {
  background-color: hotpink;
  font-family: sans-serif;
} 
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Fetching Doctor's list</title>
</head>
<body>
<a href="adminpage"> Back to Home</a><br><br>
	<h4><u>Feedback's list</u></h4>
	<c:if test="${not empty userList}">
		<table border="1">
			<tr>
			<th> <h4 >Patient name </h4></th>
				<th><h4 >email</h4></th>
				<th><h4 >state</h4></th>
				<th><h4 >district</h4></th>
				<th><h4 >country</h4></th>
				<th><h4 >feedbacktype</h4></th>
				<th><h4 >feedbackmessage</h4></th>
				<th><h4 >doctorname</h4></th>
				<th><h4 >mobileno</h4></th>
				
			
				<c:forEach var="listValue" items="${userList}">
<tr>
					<td><h4 ><c:out value="${listValue.fullname}" /> <br></h4></td>
					<td><h4 ><c:out value="${listValue.email}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.state}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.district}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.country}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.feedbacktype}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.feedbackmessage}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.doctorname}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.mobileno}" /><br></h4></td>
					</tr>
			</c:forEach>
			
		</table>
	</c:if>
	<br>
	<br>


</body>
</html>