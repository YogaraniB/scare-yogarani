<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><style type="text/css">
body {
  background-color:hsla(0, 100%, 70%, 0.3);
  font-family:sans-serif;
 
}
h1 {
  color: blue;
  font-family: sans-serif;
}
a:link {
  
  font-family: sans-serif;
}

a:visited {
  background-color: white;
 font-family: sans-serif;
}

a:hover {
  background-color: white;
  font-family:sans-serif;
}

a:active {
  background-color: hotpink;
  font-family: sans-serif;
} </style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Searched doctors</title>
</head>
<body>
<a href="patientpage"> Back to Home</a><br><br>
	<h4><u>Doctors list</u></h4>
	<c:if test="${not empty userList}">
		<table border="1">
			<tr>
			<th> <h4 >Doctor name </h4></th>
				<th><h4 >emailId</h4></th>
				<th><h4 >specialist</h4></th>
				<th><h4 >gender</h4></th>
				<th><h4 >mobileno</h4></th>
				<th><h4 >address</h4></th>
				<th><h4 >city</h4></th>
				<th><h4 >fees</h4></th>
				<th><h4 >experience</h4></th>
				<th><h4 ></h4></th>
				
			
				<c:forEach var="listValue" items="${userList}">
<tr>
					<td><h4 ><c:out value="${listValue.doctorname}" /> <br></h4></td>
					<td><h4 ><c:out value="${listValue.emailId}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.specialist}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.gender}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.mobileno}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.address}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.city}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.fees}" /><br></h4></td>
					<td><h4 ><c:out value="${listValue.experience}" /><br></h4></td>
					<td><h4 ><a href="book">book</a><br></h4></td>
					</tr>
			</c:forEach>
			
		</table>
	</c:if>
	<br>
	<br>

	


</body>
</html>