package com.asminds.Taskman.DAO;
import java.util.List;

import com.asminds.Taskman.pojo.Pojo;;
public interface DAO {
public boolean save(Pojo p);
public List<Pojo> viewAll();
public List<Pojo> view(Pojo p);
public boolean delete(Pojo p);
public List<Pojo> update(Pojo p);
}
