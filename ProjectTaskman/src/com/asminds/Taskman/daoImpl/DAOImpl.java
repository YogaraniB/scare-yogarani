package com.asminds.Taskman.daoImpl;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.asminds.Taskman.DAO.DAO;
import com.asminds.Taskman.pojo.Pojo;


import sun.print.resources.serviceui_es;


public class DAOImpl implements DAO {

	@Override
	public boolean save(Pojo p) {
		// TODO Auto-generated method stub
		Configuration cfg=new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory factory=cfg.buildSessionFactory();
		Session session=factory.openSession();
		Transaction tx=session.beginTransaction();
		session.save(p);
		tx.commit();
		session.close();
		factory.close();
		System.out.println("Object Saved Successfully");
		return true;
	}

	@Override
	public List<Pojo> viewAll() {
		Configuration cfg=new Configuration();
		cfg.configure("hibernate.cfg.xml");

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Query qry=session.createQuery("from Pojo p");
		List l=qry.list();
		System.out.println("Total Number Of Records : " + l.size());
		Iterator it = l.iterator();

		while (it.hasNext()) {
			Object o = (Object) it.next();
Pojo p=(Pojo) o;
System.out.println("Id : " +p.getId());
System.out.println("Name : " +p.getName());
System.out.println("Gender : " +p.getGender());
System.out.println("City : " +p.getCity());
System.out.println("----------------------");
		
		}
		return l;
	}

	@Override
	public List<Pojo>  view(Pojo p) {
		// TODO Auto-generated method stub
		Configuration cfg=new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory factory=cfg.buildSessionFactory();
		Session session=factory.openSession();
		Transaction tx=session.beginTransaction();
		Query qry=session.createQuery("from Pojo where id="+p.getId());
		List l=qry.list();
		System.out.println("Total Number Of Records : " + l.size());
		Iterator it = l.iterator();
System.out.println("hiiii");
		while (it.hasNext()) {
			Pojo o=(Pojo)it.next();
			System.out.println("Id : " + o.getId());
			System.out.println("Name : " + o.getName());
			System.out.println("Gender : " + o.getGender());
			System.out.println("City : " + o.getCity());
			System.out.println("----------------------");
		}
		session.close();
		factory.close();
		return l;
	}

	@Override
	public boolean delete(Pojo p) {
		// TODO Auto-generated method stub
		Configuration cfg=new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory factory=cfg.buildSessionFactory();
		Session session=factory.openSession();
		Transaction tx= session.beginTransaction();
		//Pojo p=new Pojo(p.getId());
		session.delete(p);
		System.out.println("Object deleted Successfully!");
		tx.commit();
		session.close();
		factory.close();
		
		return true;
	}
	@Override
public List<Pojo> update(Pojo j) {
		// TODO Auto-generated method stub
		int i=j.getId();
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		
		Transaction tx = session.beginTransaction();
		
		Query qry=session.createQuery("update Pojo set name=:name , gender=:gender ,city=:city where id=:id");
		qry.setParameter("id", j.getId());
		qry.setParameter("name", j.getName());
		qry.setParameter("gender", j.getGender());
		qry.setParameter("city", j.getCity());
		int result=qry.executeUpdate();
		System.out.println("Rows Affected:" +result);
		qry=session.createQuery("from Pojo where id="+j.getId());
		List l=qry.list();
		System.out.println("Total Number Of Records : " + l.size());
		Iterator it = l.iterator();
		while (it.hasNext()) {
			Pojo p=(Pojo)it.next();
			System.out.println("Id : " + p.getId());
			System.out.println("Name : " + p.getName());
			System.out.println("Gender : " + p.getGender());
			System.out.println("City : " + p.getCity());
			System.out.println("----------------------");
		}
		tx.commit();
		session.close();
		factory.close();
		return l;
	}

}
