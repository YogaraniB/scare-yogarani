package com.asminds.Taskman.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity

public class Pojo {
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
int id;
String name;
String gender;
String city;
public Pojo(int id, String name, String gender, String city) {
	super();
	this.id = id;
	this.name = name;
	this.gender = gender;
	this.city = city;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public Pojo(int id) {
	super();
	this.id = id;
}
public Pojo() {
	super();
}

}
