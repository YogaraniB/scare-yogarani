package com.asminds.Taskman.controller;

import java.util.List;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.asminds.Taskman.daoImpl.DAOImpl;
import com.asminds.Taskman.pojo.Pojo;

@org.springframework.stereotype.Controller
public class Controller {
	@RequestMapping("/")
	public String index() {
		System.out.println(" Im in index method");
		return "index";

	}

	@RequestMapping("/register")
	public String register() {
		System.out.println(" Im in register method");
		return "registerpage";

	}

	@RequestMapping("/registervalidation")
	public String registervalidation(@ModelAttribute("t") Pojo ad) {

		DAOImpl a = new DAOImpl();
		boolean b = a.save(ad);
		System.out.println("I am in registervalidation page");
		if (b == true) {

			return "registersuccess";
		} else {
			return "error";
		}
	}
	@RequestMapping("/viewall")
	public ModelAndView viewall() {

		DAOImpl a = new DAOImpl();
		List<Pojo> b=a.viewAll();
		System.out.println("I am in viewall page");
		return new ModelAndView("viewAllPage","list",b);
		
	}
	@RequestMapping("/updatesuccess")
	public String update(@ModelAttribute("t") Pojo p) {

		DAOImpl a = new DAOImpl();
		List b=a.update(p);
		System.out.println("I am in update page");
		if(b.size()!=0)
		return "updatesuccess";
		else return "updateerror";
	}
	@RequestMapping("/delete")
	public String delete() {
		System.out.println(" Im in index method");
		return "deletebyid";

	}
	@RequestMapping("/deletesuccess")
	public String deletecheck(@ModelAttribute("t") Pojo p) {

		System.out.println(p.getId());
		DAOImpl a = new DAOImpl();
		boolean b=a.delete(p);
		System.out.println("I am in delete page");
		if(b==true)
		return "deletesuccess";
		else return "deleteerror";
	}
	@RequestMapping("/search")
	public String viewone() {
		System.out.println(" Im in view method");
		return "viewbyid";

	}
	@RequestMapping("/viewoneid")
	public ModelAndView viewoneid(@ModelAttribute("t") Pojo p) {

		System.out.println(p.getId());
		int b=p.getId();
		DAOImpl a = new DAOImpl();
		List l=a.view(p);
		System.out.println("I am in viewone page");
		if(l.size()!=0)
		return new ModelAndView("viewone","list",l);
		else return new ModelAndView("viewerror","i",b);
	}
	@RequestMapping("/update")
	public String update() {
		System.out.println(" Im in update method");
		return "updatebyid";
}
	@RequestMapping("/updatesuccessid")
	public ModelAndView updateoneid(@ModelAttribute("t") Pojo p) {

		System.out.println(p.getId());
		int i=p.getId();
		DAOImpl a = new DAOImpl();
		List l=a.update(p);
		System.out.println("I am in updateone page");
		if(l.size()!=0)
		return new ModelAndView("updatesuccess","list",l);
		else return new ModelAndView("updateerror","i",i);
	}	
}
