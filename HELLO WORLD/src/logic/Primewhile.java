package logic;
import java.util.Scanner;

public class Primewhile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the Number");
		int num=s.nextInt();
		int i = 2;
        boolean flag = false;
        while(i <= num/2)
        {
            if(num % i == 0)
            {
                flag = true;
                break;
            }
           i++;
        }
        if (!flag)
            System.out.println(num + " is a prime number.");
        else
            System.out.println(num + " is not a prime number.");
    }
}
