package logic;

import java.util.Scanner;

public class PrimeNumber {
public static void main(String[] args) {
	Scanner s=new Scanner(System.in);
	int n=s.nextInt();
	boolean flag=true;
	for(int i=2;i<n/2;i++) {
		if(n%i==0) {
			flag=false;
			break;
		}
	}
if(flag==true)
	System.out.println(n +" " +" is a Prime Number");
else
	System.out.println(n +" " +" is Not a Prime Number");
}
}
