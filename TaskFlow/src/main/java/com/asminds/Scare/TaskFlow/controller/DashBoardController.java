package com.asminds.Scare.TaskFlow.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class DashBoardController {
	
	@RequestMapping("/index")
	public String index() {
		System.out.println("im index");
		return "index";
	}
	@RequestMapping("/employee")
	public String employee() {
		System.out.println("im employee");
		return "employee";
	}
	@RequestMapping("/client")
	public String client() {
		System.out.println("im client");
		return "client";
	}
	@RequestMapping("/site")
	public String site() {
		System.out.println("im site");
		return "site";
	}
	@RequestMapping("/jobs")
	public String jobs() {
		System.out.println("im jobs");
		return "jobs";
	}
	@RequestMapping("/ticket")
	public String ticket() {
		System.out.println("im ticket");
		return "ticket";
		
	}
	@RequestMapping("/asset")
	public String asset() {
		System.out.println("im asset");
		return "asset";
		
	}
	
}
