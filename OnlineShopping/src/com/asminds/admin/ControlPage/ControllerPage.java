package com.asminds.admin.ControlPage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


import com.asminds.Imp.ImplemePage.HiberImpl;
import com.asminds.pojo.PojoPage.FlowPojo;
import com.asminds.pojo.PojoPage.FrutPojo;
//import com.asminds.pojo.PojoPage.BillingPojo;
import com.asminds.pojo.PojoPage.IndexPagePojo;
import com.asminds.pojo.PojoPage.PojoClass;
import com.asminds.pojo.PojoPage.VegPojo;

@Controller
public class ControllerPage {
	@RequestMapping("/")
	public String a()
	{
		System.out.println("am index");
		return "index";
	}
	@RequestMapping("/index")
	public String index()
	{
		System.out.println("am index");
		return "index";
	}
	
	@RequestMapping("/indexx")
	public String b(@ModelAttribute("z")IndexPagePojo i)
	{
		System.out.println("i am index page form action");
		System.out.println(i.getId());
		System.out.println(i.getEmail());
		System.out.println(i.getPassword());
		
		HiberImpl l = new HiberImpl();
		l.insert(i);
		
		return "Shopping";
	}


	@RequestMapping("/ForgottenPassword")
	public String aa()
	{
		System.out.println("am fpassword");
		return "ForgottenPassword";
	}
	
	@RequestMapping("/fpass")
	public String ac()
	{
		System.out.println("am fpass");
		return "Success";
	}
	

	@RequestMapping("/indexxx")
	public String b(@ModelAttribute("y")PojoClass p)
	{
		System.out.println("i am indexxxx page form action");
		System.out.println(p.getId());
		System.out.println(p.getName());
		System.out.println(p.getEmail());
		System.out.println(p.getPassword());
		System.out.println(p.getPnumber());
		System.out.println(p.getAddress());
		System.out.println(p.getCity());
		//System.out.println(p.getPincode());
		System.out.println(p.getState());
		System.out.println(p.getLandmark());
		//System.out.println(p.getPost());
	
	
		HiberImpl l = new HiberImpl();
		l.insert1(p);
		
		return "Shopping";
	}
	

	@RequestMapping("/Shopping")
	public String a2()
	{
		System.out.println("am shapping page");
		return "Shopping";
	}

	@RequestMapping("/automobiles")
	public String a3()
	{
		System.out.println("am Automobiles");
		return "automobiles";
	}

	@RequestMapping("/auto")
	public String a4(@ModelAttribute("h")VegPojo v)
	{
		System.out.println("am auto");
		HiberImpl i = new HiberImpl();
		i.insert2(v);
		return "Card";
	}
	
	@RequestMapping("/electronics&Computers")
	public String aa5()
	{
		System.out.println("am electronics&Computers");
		return "electronics&Computers";
	}

	@RequestMapping("/fru")
	public String av(@ModelAttribute("k")FrutPojo f)
	{
		System.out.println("am fru");
		HiberImpl l = new HiberImpl();
		l.insert3(f);
		return "Card";
	}
	
	@RequestMapping("/homeappliances")
	public String aa5d()
	{
		System.out.println(" I am homeappliances");
		return "homeappliances";
	}

	@RequestMapping("/Flow")
	public String ava(@ModelAttribute("t")FlowPojo fl)
	{
		System.out.println("am Flow");
		HiberImpl l = new HiberImpl();
		l.insert4(fl);
		return "Card";
	}
	/*
	@RequestMapping("/Card")
	public String a5()
	{
		System.out.println("am index");
		return "Card";
	}*/

	@RequestMapping("/ccard")
	public String akk()
	{
		System.out.println("am card");
		return "Success";
	}
	
	@RequestMapping("/dcard")
	public String akkk()
	{
		System.out.println("am dcard");
		return "Success";
	}
	
	@RequestMapping("/acard")
	public String akkkk()
	{
		System.out.println("am acard");
		return "Success";
	}
	
	/*@RequestMapping("/Success")
	public String akkkkk()
	{
		System.out.println("am card");
		return "payment";
	}*/
	@RequestMapping("/paymentt")
	public String akkkkkk()
	{
		System.out.println("am card");
		return "payment";
	}
	
	
}


