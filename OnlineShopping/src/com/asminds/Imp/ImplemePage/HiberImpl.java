package com.asminds.Imp.ImplemePage;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.asminds.dao.DAOcontrol.DaoPage;
import com.asminds.pojo.PojoPage.FlowPojo;
import com.asminds.pojo.PojoPage.FrutPojo;
//import com.asminds.pojo.PojoPage.BillingPojo;
import com.asminds.pojo.PojoPage.IndexPagePojo;
import com.asminds.pojo.PojoPage.PojoClass;
import com.asminds.pojo.PojoPage.VegPojo;

public class HiberImpl implements DaoPage{
	
	public boolean insert(IndexPagePojo i)
	{
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml"); 

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		//PojoClass y=new PojoClass();

		Transaction tx = session.beginTransaction();
		session.save(i);
		System.out.println("Object saved successfully.....!!");
		tx.commit();
		session.close();
		factory.close();
		return true;
	}
	
	
	
	/*public boolean insert(BillingPojo b)
	{
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml"); 

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		//PojoClass y=new PojoClass();

		Transaction tx = session.beginTransaction();
		session.save(b);
		System.out.println("Object saved successfully.....!!");
		tx.commit();
		session.close();
		factory.close();
		return true;
	}*/

	public boolean insert1(PojoClass p) {
		
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml"); 

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
	

		Transaction tx = session.beginTransaction();
		session.save(p);
		System.out.println("new userdata saved successfully.....!!");
		tx.commit();
		session.close();
		factory.close();
		return true;
		
	}



	public boolean insert2(VegPojo v) {
		
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml"); 

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
	

		Transaction tx = session.beginTransaction();
		session.save(v);
		System.out.println("new userdata saved successfully.....!!");
		tx.commit();
		session.close();
		factory.close();
		return true;
	}



	public boolean insert3(FrutPojo f) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml"); 

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
	

		Transaction tx = session.beginTransaction();
		session.save(f);
		System.out.println("new userdata saved successfully.....!!");
		tx.commit();
		session.close();
		factory.close();
		return true;
	}



	public boolean insert4(FlowPojo fl) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml"); 

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
	

		Transaction tx = session.beginTransaction();
		session.save(fl);
		System.out.println("new userdata saved successfully.....!!");
		tx.commit();
		session.close();
		factory.close();
		return true;
	}


/*
	public boolean insert2(BillingPojo b) {
	
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml"); 

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		//PojoClass y=new PojoClass();

		Transaction tx = session.beginTransaction();
		session.save(b);
		System.out.println("Object saved successfully.....!!");
		tx.commit();
		session.close();
		factory.close();
		return true;
	}*/

}

