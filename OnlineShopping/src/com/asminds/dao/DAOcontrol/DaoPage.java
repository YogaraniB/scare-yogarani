package com.asminds.dao.DAOcontrol;



import com.asminds.pojo.PojoPage.FlowPojo;
import com.asminds.pojo.PojoPage.FrutPojo;
import com.asminds.pojo.PojoPage.IndexPagePojo;
import com.asminds.pojo.PojoPage.PojoClass;
import com.asminds.pojo.PojoPage.VegPojo;

public interface DaoPage {
	
	public boolean insert1(PojoClass p);
	//public boolean insert2(BillingPojo b);
	public boolean insert(IndexPagePojo i);
	public boolean insert2(VegPojo v);
	public boolean insert3(FrutPojo f);
	public boolean insert4(FlowPojo fl);

	
}
