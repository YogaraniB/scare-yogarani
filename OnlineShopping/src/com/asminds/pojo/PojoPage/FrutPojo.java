package com.asminds.pojo.PojoPage;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="FruitProduct")

public class FrutPojo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private int id;
	private String fruits;
	private String Qty;
	private String rate;
	private String total;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFruits() {
		return fruits;
	}
	public void setFruits(String fruits) {
		this.fruits = fruits;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	
	public FrutPojo() {
		
	}
	
	public FrutPojo(String fruits, String qty, String rate, String total) {
		super();
		this.fruits = fruits;
		Qty = qty;
		this.rate = rate;
		this.total = total;
	}
	public FrutPojo(int id, String fruits, String qty, String rate, String total) {
		super();
		this.id = id;
		this.fruits = fruits;
		Qty = qty;
		this.rate = rate;
		this.total = total;
	}

	
	
}
