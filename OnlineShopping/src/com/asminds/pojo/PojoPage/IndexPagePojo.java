package com.asminds.pojo.PojoPage;



import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

//import org.hibernate.mapping.Set;



@Entity
@Table(name="AlreadyUser")
public class IndexPagePojo {
	
	@Id
	@GeneratedValue
	//@OneToOne(targetEntity=PojoClass.class,cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	//@JoinColumn(name="email",referencedColumnName="email")
    //private Set<PojoClass> name;
	private int id;
	private String email;
	private String password;
	
	@Override
	public String toString() {
		return "IndexPagePojo [id=" + id + ", email=" + email + ", password=" + password + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public IndexPagePojo()
	{
		
	}
	public IndexPagePojo(int id, String email, String password) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
	}
	
	
}
