<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Appliances</title>
</head>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
function calculatePrice(){
    var price = $("#fId").val();
    var qty = $("#fQty").val();
    var total = qty*price;
    $("#fRate").val(price);
    $("#totalPrice").val(total);
}
</script>
<body bgcolor="#BDBDBD">
<form action="Flow" method="post">
<center><h4>Home Appliances</h4>

 Home Appliances:
 <select id="fId" onchange="calculatePrice()" name="flowers"> 
<option></option>
<option value="30000">Epson L655 printer</option> 
<option value="50000">Canon ImageClass MF249dw all-in-one printer</option> 
<option value="50000">Samsung RS21HZLMR1/XT 585 L Side by Side Refrigerator</option> 
<option value="25000">LG GC-D432HLAM 426 Litres Double Door</option>
 <option value="20000">Godrej RT EON 350 SD 4.4 Refrigerator</option> 
 <option value="20000">IFB Senorita Aqua VX washing machine</option> 
 <option value="10000">Bosch WAK24168IN washing machine</option> 
<option value="55000">LG MJ3286BRUS All in One Microwave Oven</option> 
<option value="22000">IFB 30BRC2 30Ltrs Microwave</option>
 </select><br><br>
 Qty:<select id="fQty" onchange="calculatePrice()" name="Qty">
 <option></option>
 <option value="1">1 </option>
 <option value="2">2 </option>
 <option value="3">3 </option>
 <option value="4">4 </option>
 <option value="5">5 </option>

 </select><br><br>
 Rate:<input type="number" id ="fRate" name="rate" ><br><br>
 Total:<input type="number" id="totalPrice" name="total"><br><br>
 
<input type="submit" value="payment"><br><br>
 <a href="Shopping">Add Items</a> 
 </form>
</body>
</html>